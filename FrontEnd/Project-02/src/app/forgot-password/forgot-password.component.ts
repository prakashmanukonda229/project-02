// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-forgot-password',
//   templateUrl: './forgot-password.component.html',
//   styleUrl: './forgot-password.component.css'
// })
// export class ForgotPasswordComponent {

// }

import { Component } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  email: string = '';

  constructor() { }

  submitForgotPasswordForm() {
    // Implement your logic here to send a password reset email to the provided email address
    console.log('Email submitted for password reset:', this.email);
    // Clear the email field after submission if needed
    this.email = '';
  }
}
