package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDao {

	@Autowired
	CustomerRepository customerRepository;

	public List<Customer> getAllCustomer() {
		return customerRepository.findAll();
	}

	public Customer registerCustomer(Customer customer) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encryptedPassword = passwordEncoder.encode(customer.getPassword());
		customer.setPassword(encryptedPassword);
		
		return customerRepository.save(customer);
	}

	public Customer customerLogin(String emailId, String password) {
		
		Customer customer = customerRepository.findByEmail(emailId);
		
		if(customer != null){
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, customer.getPassword())) {
	            
	            return customer;
	        }
		}
		
		return null;
	}


}

