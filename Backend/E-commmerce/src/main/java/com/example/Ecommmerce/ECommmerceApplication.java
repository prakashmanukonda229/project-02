package com.example.Ecommmerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ECommmerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommmerceApplication.class, args);
	}

}
