import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  loginStatus: boolean;

  // private baseUrl = 'http://localhost:8085'; 

  // private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  constructor(private http: HttpClient) { this.loginStatus = false;}

  registerCustomer(customer: any){
    return this.http.post('http://localhost:8085/registerCustomer', customer);
  }

  customerLogin(emailId:any, password:any){
    return this.http.get('http://localhost:8085/customerLogin/'+ emailId + '/' + password).toPromise();
  }


  // isUserLoggedIn() {
  //   localStorage.setItem('loggedIn', 'true');
  //   this.loggedInStatus = true;
  // }

  isCustomerLoggedIn(){
    this.loginStatus = true;
  }

  isCustomerLoggedOut(){
    this.loginStatus = false;
  }

  getCustomerLoginStatus(): boolean{
    return this.loginStatus;
  }
}
