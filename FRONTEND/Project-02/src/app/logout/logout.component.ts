import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {

  constructor(private service: CustomerService, private router:Router, private toastr: ToastrService){
    
    this.service.isCustomerLoggedOut();   // Status false
    this.toastr.warning(
      "Logged Out", 
      'Login Status', 
      { timeOut: 4000, progressBar:true, progressAnimation:'increasing'
    });
    this.router.navigate(['login']);
  }


}
