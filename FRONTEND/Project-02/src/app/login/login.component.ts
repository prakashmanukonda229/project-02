import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  customer:any;

  constructor(private service: CustomerService , private toastr: ToastrService, private router: Router){}

  async loginSubmit(loginForm:any){

    if(loginForm.emailId == 'admin' && loginForm.password == 'admin'){
      
    }else{
      await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data:any) =>{
          this.customer = data;
        });
      
      if(this.customer != null){
        this.toastr.success(
          "Login Successfully", 
          'Login Status', 
          { timeOut:3000, progressBar : true, progressAnimation : 'increasing'
        });
        this.service.isCustomerLoggedIn();
        this.router.navigate(['']);
      }else{
        this.toastr.error("Invalid Credentials", 'Login Status', { timeOut:5000, progressBar: true, progressAnimation:'increasing'});
      }
      
    }
  }
}





































































































































































































































































// import { Component } from '@angular/core';
// import { Router } from '@angular/router';
// import { CustomerserviceService } from '../customer.service';
// import { ToastrService } from 'ngx-toastr';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent {
//   email: string = '';
//   password: string = '';

//   constructor(private router: Router, private service: CustomerserviceService, private tostr: ToastrService) {}

//   async loginSubmit(loginForm: any) {
//     localStorage.setItem('email', loginForm.email);

//     if (loginForm.email === 'Admin' && loginForm.password === 'Admin') {
     
//     } else {
//       try {
//         const data = await this.service.customerLogin(loginForm.email, loginForm.password).toPromise();
//         if (data) {
//           this.service.isCustomerLoggedIn(true); 
//           this.router.navigate(['']);
//         } else {
//           alert('Invalid Credentials');
//           this.router.navigate(['login']);
//         }
//       } catch (error) {
//         console.error(error);
//         alert('Login failed. Please try again.');
//       }
//     }
//   }

//   logout(): void {
//     this.service.setLoggedIn(false); 
//     this.router.navigate(['/login']);
//   }
// }