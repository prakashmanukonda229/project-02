import { TeacherUpdateStudentComponent } from './components/teacher/teacher-update-student/teacher-update-student.component';
import { WelcomeComponent } from './components/admin/welcome/welcome.component';
import { StudentResultComponent } from './components/student/student-result/student-result.component';
import { AdminAnalyticsComponent } from './components/admin/admin-analytics/admin-analytics.component';
import { AdminResultViewComponent } from './components/admin/admin-result-view/admin-result-view.component';
import { AdminClassDetailComponent } from './components/admin/admin-class-detail/admin-class-detail.component';
import { TeacherDetailsComponent } from './components/admin/teacher-details/teacher-details.component';
import { AdminResultComponent } from './components/admin/admin-result/admin-result.component';
import { AdminSubjectComponent } from './components/admin/admin-subject/admin-subject.component';
import { AdminClassComponent } from './components/admin/admin-class/admin-class.component';
import { AdminTeacherComponent } from './components/admin/admin-teacher/admin-teacher.component';
import { AdminDashboardComponent } from './components/admin/admin-dashboard/admin-dashboard.component';
import { SubjectComponent } from './components/teacher/subject/subject.component';
import { AddStudentsClassComponent } from './components/teacher/add-students-class/add-students-class.component';
import { TeacherProfileComponent } from './components/teacher/teacher-profile/teacher-profile.component';
import { TeacherViewMarksComponent } from './components/teacher/teacher-view-marks/teacher-view-marks.component';
import { TeacherAddMarksComponent } from './components/teacher/teacher-add-marks/teacher-add-marks.component';
import { TeacherViewStudentsComponent } from './components/teacher/teacher-view-students/teacher-view-students.component';
import { TeacherAddStudentComponent } from './components/teacher/teacher-add-student/teacher-add-student.component';
import { TeacherDashboardComponent } from './components/teacher/teacher-dashboard/teacher-dashboard.component';
import { AdminLoginComponent } from './components/admin/admin-login/admin-login.component';
import { SecurityQuestionComponent } from './components/teacher/security-question/security-question.component';
import { MobileComponent } from './components/teacher/mobile/mobile.component';
import { ResetPasswordComponent } from './components/teacher/reset-password/reset-password.component';
import { TeacherRegisterComponent } from './components/teacher/teacher-register/teacher-register.component';
import { TeacherLoginComponent } from './components/teacher/teacher-login/teacher-login.component';
import { TeacherHomeComponent } from './components/teacher/teacher-home/teacher-home.component';
import { StudentLoginComponent } from './components/student/student-login/student-login.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'student', component: StudentLoginComponent },
  { path: 'teacher', component: TeacherHomeComponent },
  { path: 'teacher/login', component: TeacherLoginComponent },
  { path: 'teacher/register', component: TeacherRegisterComponent },
  { path: 'teacher/login/mobile', component: MobileComponent },

  {
    path: 'teacher/login/mobile/security-answer',
    component: SecurityQuestionComponent,
  },
  {
    path: 'teacher/login/mobile/security-answer/reset-password',
    component: ResetPasswordComponent,
  },
  {
    path: 'teacher/dashboard',
    component: TeacherDashboardComponent,
    children: [
      { path: '', component: WelcomeComponent },
      { path: 'add-student', component: AddStudentsClassComponent },
      { path: 'view-students', component: AddStudentsClassComponent },
      { path: 'add-marks', component: AddStudentsClassComponent },
      { path: 'view-marks', component: AddStudentsClassComponent },
      { path: 'profile', component: TeacherProfileComponent },
      { path: 'add-student/:cId', component: TeacherAddStudentComponent },
      { path: 'view-students/:cId', component: TeacherViewStudentsComponent },
      { path: 'add-marks/:cId', component: SubjectComponent },
      { path: 'add-marks/:cId/:sId', component: TeacherAddMarksComponent },
      { path: 'view-marks/:cId', component: TeacherViewMarksComponent },
      {
        path: 'view-students/:cId/update/:stuId',
        component: TeacherUpdateStudentComponent,
      },
    ],
  },

  {
    path: 'admin',
    component: AdminLoginComponent,
  },
  {
    path: 'admin/dashboard',
    component: AdminDashboardComponent,
    children: [
      { path: '', component: WelcomeComponent },
      { path: 'teachers', component: AdminTeacherComponent },
      { path: 'subjects', component: AdminSubjectComponent },
      { path: 'classes', component: AdminClassComponent },
      { path: 'results', component: AdminResultComponent },
      { path: 'teachers/:tId', component: TeacherDetailsComponent },
      { path: 'classes/:cl', component: AdminClassDetailComponent },
      { path: 'results/:cId', component: AdminResultViewComponent },
      { path: 'analytics', component: AdminAnalyticsComponent },
    ],
  },
  {
    path: 'student/result',
    component: StudentResultComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
