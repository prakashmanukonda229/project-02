import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StudentLoginComponent } from './components/student/student-login/student-login.component';
import { TeacherHomeComponent } from './components/teacher/teacher-home/teacher-home.component';
import { TeacherLoginComponent } from './components/teacher/teacher-login/teacher-login.component';
import { TeacherRegisterComponent } from './components/teacher/teacher-register/teacher-register.component';
import { ResetPasswordComponent } from './components/teacher/reset-password/reset-password.component';
import { MobileComponent } from './components/teacher/mobile/mobile.component';
import { SecurityQuestionComponent } from './components/teacher/security-question/security-question.component';
import { AdminLoginComponent } from './components/admin/admin-login/admin-login.component';
import { TeacherDashboardComponent } from './components/teacher/teacher-dashboard/teacher-dashboard.component';
import { TeacherNavigationComponent } from './components/teacher/teacher-navigation/teacher-navigation.component';
import { TeacherAddStudentComponent } from './components/teacher/teacher-add-student/teacher-add-student.component';
import { TeacherViewStudentsComponent } from './components/teacher/teacher-view-students/teacher-view-students.component';
import { TeacherAddMarksComponent } from './components/teacher/teacher-add-marks/teacher-add-marks.component';
import { TeacherViewMarksComponent } from './components/teacher/teacher-view-marks/teacher-view-marks.component';
import { TeacherProfileComponent } from './components/teacher/teacher-profile/teacher-profile.component';
import { AddStudentsClassComponent } from './components/teacher/add-students-class/add-students-class.component';
import { SubjectComponent } from './components/teacher/subject/subject.component';
import { AdminDashboardComponent } from './components/admin/admin-dashboard/admin-dashboard.component';
import { AdminNavigationComponent } from './components/admin/admin-navigation/admin-navigation.component';
import { AdminTeacherComponent } from './components/admin/admin-teacher/admin-teacher.component';
import { AdminClassComponent } from './components/admin/admin-class/admin-class.component';
import { AdminResultComponent } from './components/admin/admin-result/admin-result.component';
import { AdminSubjectComponent } from './components/admin/admin-subject/admin-subject.component';
import { TeacherDetailsComponent } from './components/admin/teacher-details/teacher-details.component';
import { AdminClassDetailComponent } from './components/admin/admin-class-detail/admin-class-detail.component';
import { AdminResultViewComponent } from './components/admin/admin-result-view/admin-result-view.component';
import { AdminAnalyticsComponent } from './components/admin/admin-analytics/admin-analytics.component';
import { StudentResultComponent } from './components/student/student-result/student-result.component';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeComponent } from './components/admin/welcome/welcome.component';
import { TeacherUpdateStudentComponent } from './components/teacher/teacher-update-student/teacher-update-student.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentLoginComponent,
    TeacherHomeComponent,
    TeacherLoginComponent,
    TeacherRegisterComponent,
    ResetPasswordComponent,
    MobileComponent,
    SecurityQuestionComponent,
    AdminLoginComponent,
    TeacherDashboardComponent,
    TeacherNavigationComponent,
    TeacherAddStudentComponent,
    TeacherViewStudentsComponent,
    TeacherAddMarksComponent,
    TeacherViewMarksComponent,
    TeacherProfileComponent,
    AddStudentsClassComponent,
    SubjectComponent,
    AdminDashboardComponent,
    AdminNavigationComponent,
    AdminTeacherComponent,
    AdminClassComponent,
    AdminResultComponent,
    AdminSubjectComponent,
    TeacherDetailsComponent,
    AdminClassDetailComponent,
    AdminResultViewComponent,
    AdminAnalyticsComponent,
    StudentResultComponent,
    WelcomeComponent,
    TeacherUpdateStudentComponent
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
