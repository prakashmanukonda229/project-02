import { Subject } from './subject';
export class TeacherClass {
  public id: number = 0;
  public name: string = '';
  public mobile: string = '';
  public username: string = '';
  public password: string = '';
  public secQue: string = '';
  public secAns: string = '';
  public active: boolean = false;
}

export class Class1 {
  id: any;
  name: any;
  teacher: TeacherClass = new TeacherClass();
  published!: boolean;
}

export class SubClassWrapper {
  classes: Class1[] = [];
  subjects: Subject[] = [];
  teacher!: TeacherClass;
  teacherNames: String[] = [];
  subjectNames: String[] = [];
  results: Result[] = [];
  resultOfClassForSem: number[][] = [];
  studentList: Student[] = [];
  confirmed!: boolean;
  markList: number[] = [];
}

export class Student {
  id: number = 0;
  rollNo!: number;
  motherName!: String;
  fullName!: String;
  class1!: Class1;
  passed!: boolean;
}

export class Result {
  id!: number;
  marks!: number;
  student!: Student;
  subject!: Subject;
}
