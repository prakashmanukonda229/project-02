import { Class1, TeacherClass } from 'src/app/Models/teacher';
import { Subject } from './subject';
export class Admin {
  public id: number = 0;
  public password: string = '';
}

export class Permission {
  public id: number = 0;
  public class1!: Class1;
  public subject!: Subject;
  public teacher!: TeacherClass;
  public confirmed!: boolean;
}
