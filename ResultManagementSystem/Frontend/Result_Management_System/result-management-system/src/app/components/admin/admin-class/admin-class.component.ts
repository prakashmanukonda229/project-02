import { TeacherServiceService } from './../../../services/teacher-service.service';
import { Class1, Student } from './../../../Models/teacher';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-class',
  templateUrl: './admin-class.component.html',
  styleUrls: ['./admin-class.component.css'],
})
export class AdminClassComponent implements OnInit {
  class1: Class1 = new Class1();
  @ViewChild('class') classValue: any;

  constructor(
    private adminService: AdminService,
    private toastr: ToastrService,
    private teacherService :TeacherServiceService
  ) {}
  buttonText = '';
  isFormVisible = false;
  isupdateVisible = false;
  classes: any[] = [];
  studentList: Student[] = [];

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.buttonText = 'Add Class';
      this.isFormVisible = false;
      this.isupdateVisible = false;
      this.getAllClasses();
    }
  }
  ///////////////////////////////////////////////////////
  public onClickAddClass() {
    if (this.isFormVisible == true) {
      this.isFormVisible = false;
      this.buttonText = 'Add Class';
    } else {
      this.isFormVisible = true;
      this.buttonText = 'Close';
    }
  }
  ////////////////////////////////////////////////////////
  public onClickUpdate() {
    this.isupdateVisible = true;
  }
  ////////////////////////////////////////////////////////
  onClickBack() {
    this.isupdateVisible = false;
    this.getAllClasses()
  }
  ////////////////////////////////////////////////////////
  public onSubmitAddForm() {
    this.adminService.addClass(this.class1).subscribe(
      (value) => {
        if (value == 0) {
          this.toastr.warning('Class already present!', '', {
            positionClass: 'toast-custom1',
          });
        } else if (value == 1) {
          this.toastr.success('Class added successfully!', '', {
            positionClass: 'toast-custom1',
          });
          this.getAllClasses();
          this.classValue.reset();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ////////////////////////////////////////////////////////
  public getAllClasses() {
    this.adminService.getClasses().subscribe(
      (data) => {
        this.classes = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ////////////////////////////////////////////////////////
  public deleteClass(cId: number) {
    this.adminService.deleteClass(cId).subscribe(
      (data) => {
        this.getAllClasses();
        this.toastr.warning('Class Deleted', '', {
          positionClass: 'toast-custom1',
        });
      },
      (error) => {
        this.toastr.error('Something Went Wrong while deleting', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  //////////////////////////////////////////////////////////////////////////
  public processDelete(cId: number) {
    this.getStudentsOfClass(cId);
  }
  /////////////////////////////////////////////////////////////////////////
  getStudentsOfClass(cId: any) {
    this.teacherService.studentsOfClass(cId).subscribe(
      (value: any) => {
        this.studentList = value;
        this.confirmDeletion(this.studentList.length, cId);
      },
      (error) => {
        this.toastr.error('problem occured while loading Class!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  //////////////////////////////////////////////////////////////////////////
  confirmDeletion(listlength: any, cId: number) {
    if (listlength > 0) {
      let textt =
        'This action will also delete ' +
        listlength +
        ' students of this class from records!';
      Swal.fire({
        title: 'Warning!',
        text: textt,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete Anyhow!',
        cancelButtonText: 'Cancel',
      }).then((result) => {
        if (result.value) {
          this.deleteClass(cId);
        } else {
        }
      });
    } else {
      this.deleteClass(cId);
    }
  }
  ///////////////////////////////////////////////////////////////////////
  public updateClass(class1: Class1) {
    this.adminService.updateClass(class1).subscribe(
      (data) => {
        if (data == 0) {
          this.toastr.warning('Class already present!', '', {
            positionClass: 'toast-custom1',
          });
        } else {
          this.toastr.success('Class Updated successfully!', '', {
            positionClass: 'toast-custom1',
          });
          this.getAllClasses();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong while Updating Class', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
/////////////////////////////////////////////////////////////////////////
