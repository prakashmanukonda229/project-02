import { Class1 } from './../../../Models/teacher';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-admin-result',
  templateUrl: './admin-result.component.html',
  styleUrls: ['./admin-result.component.css'],
})
export class AdminResultComponent implements OnInit {
  constructor(
    private toastr: ToastrService,
    private adminService: AdminService
  ) {}

  classList: Class1[] = [];

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.getAllClasses();
    }
  }
////////////////////////////////////////////////////////////////////////////
  public getAllClasses() {
    this.adminService.getClasses().subscribe(
      (value: any) => {
        this.classList = value;
      },
      (error) => {
        this.toastr.error('Error in loading all classes', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
