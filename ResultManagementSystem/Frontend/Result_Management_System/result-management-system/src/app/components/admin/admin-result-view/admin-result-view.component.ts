import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/Models/Admin';
import { Subject } from 'src/app/Models/subject';
import { Class1, Student } from 'src/app/Models/teacher';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-admin-result-view',
  templateUrl: './admin-result-view.component.html',
  styleUrls: ['./admin-result-view.component.css'],
})
export class AdminResultViewComponent implements OnInit {
  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,
  ) {}

  classId!: number;
  class1!: Class1;
  finResForSemList: number[][] = [[]];
  studenList: Student[] = [];
  subjectList: Subject[] = [];
  proPer: number = 0;
  permissions: Permission[] = [];
  showDetails: boolean = false;
  dtext: String = 'DETAILS';
  perLocal: Permission[] = [];

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.classId = this.route.snapshot.params['cId'];
      this.getClassDetails();
      this.showDetails = false;
      this.dtext = 'DETAILS';
    }
  }
///////////////////////////////////////////////////////////////////////
  getClassDetails() {
    this.adminService.getClassDetails(this.classId).subscribe(
      (value: any) => {
        this.class1 = value;
        this.getResultForClassAndSem();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getResultForClassAndSem() {
    this.teacherService.getResultForClass(this.classId).subscribe(
      (value: any) => {
        this.finResForSemList = value.resultOfClassForSem;
        this.studenList = value.studentList;
        this.subjectList = value.subjects;
        this.getPermissions();
      },
      (error) => {
        this.toastr.error('problem occured while loading class Result', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getPermissions() {
    this.adminService.getPermissions(this.classId).subscribe(
      (value: any) => {
        this.permissions = value;
        this.perLocal = this.permissions.filter((p) => p.confirmed);
        if (this.permissions.length > 0) {
          this.proPer = Math.round(
            (this.perLocal.length * 100) / this.permissions.length
          );
        } else {
          this.proPer = 0;
        }
      },
      (error) => {
        this.toastr.error('problem occured while loading details', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  onClickDetail() {
    if (this.showDetails) {
      this.showDetails = false;
      this.dtext = 'DETAILS';
    } else {
      this.showDetails = true;
      this.dtext = 'BACK';
    }
  }

  publish() {
    this.adminService.publish(this.class1).subscribe(
      (value: any) => {
        this.toastr.success(
          'Result published for Class ' + this.class1.name,
          '',
          { positionClass: 'toast-custom1' }
        );
        this.getResultForClassAndSem();
        this.getClassDetails();
      },
      (error) => {
        this.toastr.error('problem occured while loading details', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  revert() {
    this.adminService.revert(this.class1).subscribe(
      (value: any) => {
        this.toastr.success(
          'Result reverted for Class ' + this.class1.name,
          '',
          { positionClass: 'toast-custom1' }
        );
        this.getClassDetails();
      },
      (error) => {
        this.toastr.error('problem occured while loading details', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
