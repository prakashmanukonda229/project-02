import { Subject } from './../../../Models/subject';
import { TeacherServiceService } from './../../../services/teacher-service.service';
import {
  Class1,
  SubClassWrapper,
  TeacherClass,
} from './../../../Models/teacher';
import { Permission } from './../../../Models/formmodel';
import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-teacher-details',
  templateUrl: './teacher-details.component.html',
  styleUrls: ['./teacher-details.component.css'],
})
export class TeacherDetailsComponent implements OnInit {
  subClassWrapper: SubClassWrapper = new SubClassWrapper();
  teacherId: any = 0;
  teacher: TeacherClass = new TeacherClass();
  showdetails: Boolean = true;
  editStudent: Boolean = false;
  editMarks: Boolean = false;
  classList: any[] = [];
  classes: Class1[] = [];
  allClasses: Class1[] = [];
  classIndexes: number[] = [];
  subjects: Subject[] = [];

  tempSubList: Subject[] = [];
  tempClassList: Class1[] = [];
  subOne: any;
  clOne: any;
  permission: Permission = new Permission();
  permissions: Permission[] = [];

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.teacherId = this.route.snapshot.params['tId'];
      this.teacher.id = this.teacherId;
      this.getTecherDetails();
      this.getClasses();
      this.getClassesForClassTeacher();
      this.getPermissionsForTeacher();
      this.getAllClasses();
      this.clOne = '--Select--';
      this.subOne = '--Select--';
    }
  }

  editStudentPermission() {
    this.showdetails = false;
    this.editStudent = true;
    this.editMarks = false;
  }

  editMarksPermission() {
    this.showdetails = false;
    this.editStudent = false;
    this.editMarks = true;
  }
  onSubmit() {
    this.showdetails = true;
    this.editStudent = false;
    this.editMarks = false;
  }

  onSubmitMarks() {
    this.showdetails = true;
    this.editStudent = false;
    this.editMarks = false;
  }

  public getPermissionsForTeacher() {
    this.adminService.getPermissionsForTeacher(this.teacher).subscribe(
      (value: any) => {
        this.permissions = value;
        this.tempSubList = [];
        this.subOne = '--Select--';
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  onAddPermission() {
    this.permission.teacher = this.teacher;
    this.permission.class1 = this.tempClassList[0];
    this.permission.subject = this.tempSubList[0];

    this.adminService.addPermission(this.permission).subscribe(
      (value) => {
        if (value == 'SUCCESS') {
          this.getPermissionsForTeacher();
          this.toastr.success('Permission added!', '', {
            positionClass: 'toast-custom1',
          });
        } else if (value == 'ALREADY') {
          this.toastr.info('Permission already present!', '', {
            positionClass: 'toast-custom1',
          });
        } else {
          this.toastr.warning(
            'Selected class and subjects is already assigned to Teacher ' +
              value,
            '',
            { positionClass: 'toast-custom1' }
          );
        }
      },
      (error) => {
        console.log(error);
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public onSelectClassForPermission(clas: any) {
    this.tempClassList = this.allClasses.filter(
      (cl) => cl.name == clas.target.value
    );

    this.adminService.getSubjectForClasses(this.tempClassList[0]).subscribe(
      (subjects: any) => {
        this.subjects = subjects;
        if (this.subjects.length == 0) {
          this.toastr.info(
            'No subject is assigned to class ' + this.tempClassList[0].name,
            '',
            { positionClass: 'toast-custom1' }
          );
          this.subOne = '--Select--';
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public onSelectSubForPermission(sub: any) {
    this.tempSubList = this.subjects.filter(
      (subt) => subt.name == sub.target.value
    );
  }

  public onSelectClass(event: any, classItem: Class1) {
    if (event.target.checked) {
      this.classes.push(classItem);
      this.classIndexes.push(classItem.id);
    } else {
      this.classes = this.classes.filter((c: any) => c.id != classItem.id);
      this.classIndexes = this.classIndexes.filter(
        (c: any) => c != classItem.id
      );
    }
  }

  getTecherDetails() {
    this.teacherService.getTeacherById(this.teacher).subscribe(
      (value) => {
        this.teacher = value;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getClasses() {
    this.adminService.getClassesForTeacher(this.teacher).subscribe(
      (value) => {
        this.classList = value;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getAllClasses() {
    this.adminService.getClasses().subscribe(
      (value: any) => {
        this.allClasses = value;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public deactivateTeacher() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This will delete all permissions of the Teacher ',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Deactivate.',
      cancelButtonText: 'No, let me think',
    }).then((result) => {
      if (result.value) {
        this.adminService.deactivateTeacher(this.teacher).subscribe(
          (value) => {
            if (value == 0) {
              this.toastr.info(
                'Teacher Deactivated! All permissions are deleted',
                '',
                { positionClass: 'toast-custom1' }
              );
              this.getTecherDetails();
              this.getPermissionsForTeacher();
            }
          },
          (error) => {
            this.toastr.error('Something Went Wrong!', '', {
              positionClass: 'toast-custom1',
            });
          }
        );
      }
    });
  }

  public activateTeacher() {
    this.adminService.activateTeacher(this.teacher).subscribe(
      (value) => {
        if (value == 0) {
          this.toastr.success('Teacher is now Active!', '', {
            positionClass: 'toast-custom1',
          });
          this.getTecherDetails();
          this.getClassesForClassTeacher();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public updateClassTeacher() {
    this.subClassWrapper.classes = this.classes;
    this.subClassWrapper.teacher = this.teacher;
    this.adminService.updateClassTeacher(this.subClassWrapper).subscribe(
      (value) => {
        if (value == 0) {
          if (this.classes.length == 0) {
            this.toastr.info('No Class Selected', '', {
              positionClass: 'toast-custom1',
            });
          } else if (this.classes.length == 1) {
            this.toastr.success(
              this.teacher.name +
                ' is now class Teacher of ' +
                this.classes[0].name +
                ' class',
              '',
              { positionClass: 'toast-custom1' }
            );
          } else {
            this.toastr.success(
              this.teacher.name +
                ' is now class Teacher of ' +
                this.classes.length +
                ' classes',
              '',
              { positionClass: 'toast-custom1' }
            );
          }
          this.getClassesForClassTeacher();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public getClassesForClassTeacher() {
    this.adminService.getClassesForClassTeacher(this.teacher).subscribe(
      (value: any) => {
        this.classes = value;
        this.classIndexes = this.classes.map((clOb) => clOb.id);
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  deletePermission(perm: Permission) {
    this.adminService.deletePermission(perm).subscribe(
      (value: any) => {
        this.getPermissionsForTeacher();
        this.toastr.info('Permission Deleted!', '', {
          positionClass: 'toast-custom1',
        });
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  deleteTeacher() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This will delete Teacher Details from Database',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete.',
      cancelButtonText: 'Not Now',
    }).then((result) => {
      if (result.value) {
        this.adminService.deleteTeacher(this.teacher).subscribe(
          (value) => {
            if (value == 0) {
              this.toastr.info('Teacher Deleted!', '', {
                positionClass: 'toast-custom1',
              });
              this.router.navigate(['/admin/dashboard/teachers']);
            }
          },
          (error) => {
            this.toastr.error('Something Went Wrong!', '', {
              positionClass: 'toast-custom1',
            });
          }
        );
      }
    });
  }
}
