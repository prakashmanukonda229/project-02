import { AdminService } from './../../../services/admin.service';
import { Admin } from './../../../Models/Admin';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css'],
})
export class AdminLoginComponent implements OnInit {
  admin: Admin = new Admin();
  constructor(
    private adminService: AdminService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  public adminLogin() {
    this.adminService.adminLoginCall(this.admin).subscribe(
      (isPresent) => {
        if (isPresent) {
          sessionStorage.setItem('isAdminLoggedIn', isPresent);
          this.toastr.success('Login Successful!', '', {
            positionClass: 'toast-custom1',
          });
          this.router.navigate(['admin/dashboard']);
        } else {
          this.toastr.warning('Wrong Password!', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
