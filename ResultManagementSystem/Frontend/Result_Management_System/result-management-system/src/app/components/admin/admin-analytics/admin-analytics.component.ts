import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-analytics',
  templateUrl: './admin-analytics.component.html',
  styleUrls: ['./admin-analytics.component.css'],
})
export class AdminAnalyticsComponent implements OnInit {
  analytics: number[] = [];

  constructor(
    private adminService: AdminService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.getData();
    }
  }

  // Get all analytical data such as number of students, techers, subjects and classes
  getData() {
    this.adminService.getData().subscribe(
      (data: any) => {
        this.analytics = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong while loading Data', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
