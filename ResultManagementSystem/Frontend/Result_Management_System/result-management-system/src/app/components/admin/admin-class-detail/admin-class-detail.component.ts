import { TeacherServiceService } from 'src/app/services/teacher-service.service';
import { Class1, Student, SubClassWrapper } from './../../../Models/teacher';
import { Subject } from './../../../Models/subject';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-class-detail',
  templateUrl: './admin-class-detail.component.html',
  styleUrls: ['./admin-class-detail.component.css'],
})
export class AdminClassDetailComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,
    private teacherService : TeacherServiceService
  ) {}

  class1: Class1 = new Class1();
  selStudent: boolean = false;
  selTeacher: boolean = false;
  selSubject: boolean = false;
  classVal: any;
  subjectsOfClass: Subject[] = [];
  selectedSubjects: Subject[] = [];
  subClassWrapper: SubClassWrapper = new SubClassWrapper();
  studentList: Student[] = [];
  subIndexes: number[] = [];
  allSubjects: any[] = [];
  viewSub: boolean = true;
  viewAddSub: boolean = false;

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.selStudent = true;
      this.selTeacher = false;
      this.selSubject = false;
      this.viewSub = true;
      this.viewAddSub = false;
      this.classVal = this.route.snapshot.params['cl'];
      this.getClassDetails();
    }
  }
  /////////////////////////////////////////////////////////////////
  public onClickStudent() {
    this.selStudent = true;
    this.selTeacher = false;
    this.selSubject = false;
    this.viewSub = true;
    this.viewAddSub = false;
  }
  //////////////////////////////////////////////////////////////
  public onClickTeacher() {
    this.selStudent = false;
    this.selTeacher = true;
    this.selSubject = false;
    this.viewSub = true;
    this.viewAddSub = false;
  }
  ///////////////////////////////////////////////////////////////
  public onClickSubject() {
    this.selStudent = false;
    this.selTeacher = false;
    this.selSubject = true;
    this.viewSub = true;
    this.viewAddSub = false;
  }
  ///////////////////////////////////////////////////////////////
  public onClickAddSub() {
    this.viewSub = false;
    this.viewAddSub = true;
    this.subjectsOfClass.forEach((sub) => this.subIndexes.push(sub.id));
    this.selectedSubjects = this.subjectsOfClass
    // this.subjectsOfClass.forEach((sub) => this.selectedSubjects.push(sub));
    
  }
  ///////////////////////////////////////////////////////////////
  onClickBack() {
    this.viewSub = true;
    this.viewAddSub = false;
    this.subIndexes = [];
    this.selectedSubjects = [];
  }
  ///////////////////////////////////////////////////////////////
  getSubjectsOfClass() {
    this.adminService.getSubjectForClasses(this.class1).subscribe(
      (value: any) => {
        this.subjectsOfClass = value;

        this.selectedSubjects = this.subjectsOfClass
        this.subIndexes = []
        this.subjectsOfClass.forEach((sub) => this.subIndexes.push(sub.id));
        
      },
      (error) => {
        this.toastr.error('problem occured while loading Subjects!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  getTeachersOfClass() {
    this.adminService.getTeachersOfClass(this.class1.id).subscribe(
      (value: any) => {
        this.subClassWrapper = value;
      },
      (error) => {
        this.toastr.error('problem occured while loading Teachers!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  public getAllSubjects() {
    this.adminService.getSubjects().subscribe(
      (data) => {
        this.allSubjects = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  getClassDetails() {
    this.adminService.getClassDetails(this.classVal).subscribe(
      (value: any) => {
        this.class1 = value;
        this.getSubjectsOfClass();
        this.getTeachersOfClass();
        this.getStudentsOfClass();
        this.getAllSubjects();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  getStudentsOfClass() {
    this.teacherService.studentsOfClass(this.classVal).subscribe(
      (value: any) => {
        this.studentList = value;
      },
      (error) => {
        this.toastr.error('problem occured while loading Class!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  public assignSubToClass() {
    this.subClassWrapper.subjects = this.selectedSubjects;
    this.subClassWrapper.classes = [this.class1];

    this.adminService.addSubToClass(this.subClassWrapper).subscribe(
      (data) => {
        if (data == 0) {
          this.getSubjectsOfClass();
          this.toastr.success(
            'subject updated for Class ' + this.class1.name,
            '',
            { positionClass: 'toast-custom1' }
          );
        } else {
          this.toastr.error(
            'Problem in adding Subjects to Class ' + this.class1.name,
            '',
            { positionClass: 'toast-custom1' }
          );
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
  ///////////////////////////////////////////////////////////////
  public onSelectSubject(event: any, sub: any) {
    if (event.target.checked) {
      this.selectedSubjects.push(sub);
      this.subIndexes.push(sub.id);
    } else {
      this.selectedSubjects = this.selectedSubjects.filter(
        (subj: any) => subj.id != sub.id
      );
      this.subIndexes = this.subIndexes.filter((s: any) => s != sub.id);
    }
  }
}
  ///////////////////////////////////////////////////////////////