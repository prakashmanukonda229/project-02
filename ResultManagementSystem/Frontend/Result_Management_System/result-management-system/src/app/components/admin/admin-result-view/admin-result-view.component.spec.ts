import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminResultViewComponent } from './admin-result-view.component';

describe('AdminResultViewComponent', () => {
  let component: AdminResultViewComponent;
  let fixture: ComponentFixture<AdminResultViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminResultViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminResultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
