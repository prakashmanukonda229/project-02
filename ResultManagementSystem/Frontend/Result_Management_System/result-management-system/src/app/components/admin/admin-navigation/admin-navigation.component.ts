import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-navigation',
  templateUrl: './admin-navigation.component.html',
  styleUrls: ['./admin-navigation.component.css'],
})
export class AdminNavigationComponent implements OnInit {
  fals: any = false;
  constructor(private router: Router, private toastr: ToastrService) {}

  ngOnInit(): void {}

  public processLogout() {
    sessionStorage.removeItem('isAdminLoggedIn');
    this.toastr.success('You are logged out!!', '', {
      positionClass: 'toast-custom1',
    });
    this.router.navigate(['']);
  }
}
