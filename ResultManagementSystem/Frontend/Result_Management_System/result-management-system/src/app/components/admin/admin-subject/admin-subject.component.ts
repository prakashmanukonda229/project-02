import { Subject } from './../../../Models/subject';
import { Class1, SubClassWrapper } from './../../../Models/teacher';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-subject',
  templateUrl: './admin-subject.component.html',
  styleUrls: ['./admin-subject.component.css'],
})
export class AdminSubjectComponent implements OnInit {
  @ViewChild('subName') subName: any;
  @ViewChild('fm') fullMarks: any;
  constructor(
    private adminService: AdminService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  subClassWrapper: SubClassWrapper = new SubClassWrapper();
  subject: Subject = new Subject();
  subjects: any[] = [];
  class: Class1 = new Class1();
  classes: any[] = [];
  selectedClasses: any[] = [];
  selSub: Subject = new Subject();
  classIndexes: number[] = [];

  showUpdate: boolean = false;
  showMain: boolean = true;
  showAssign: boolean = false;

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      this.showUpdate = false;
      this.showMain = true;
      this.showAssign = false;
      this.getAllSubjects();
      this.getAllClasses();
    }
  }
///////////////////////////////////////////////////////////////////
  public onHome() {
    this.showUpdate = false;
    this.showMain = true;
    this.showAssign = false;
    this.getAllSubjects();
    this.router.navigate(['/admin/dashboard/subjects']);
  }

  public onUpdate() {
    this.showUpdate = true;
    this.showMain = false;
    this.showAssign = false;
  }

  public onAssign() {
    this.showUpdate = false;
    this.showMain = false;
    this.showAssign = true;
  }

  public addSubject() {
    this.adminService.addSubject(this.subject).subscribe(
      (value) => {
        if (value == 0) {
          this.toastr.warning('Suject already present!', '', {
            positionClass: 'toast-custom1',
          });
        } else if (value == 1) {
          this.toastr.success('Subject added!', '', {
            positionClass: 'toast-custom1',
          });
          this.getAllSubjects();
          this.subName.reset();
          this.fullMarks.reset();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public updateSubject(upSub: Subject) {
    this.adminService.updateSubject(upSub).subscribe(
      (value) => {
        if (value == 1) {
          this.toastr.success('Subject updated!', '', {
            positionClass: 'toast-custom1',
          });
          this.getAllSubjects();
          this.subName.reset();
          this.fullMarks.reset();
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public deleteSubject(subId: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this Subject, this may also delete respective marks and permissions',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then((result) => {
      if (result.value) {
        this.adminService.deleteSubject(subId).subscribe(
          (value) => {
            if (value == 1) {
              this.toastr.success('Subject Deleted!', '', {
                positionClass: 'toast-custom1',
              });
              this.subName.reset();
              this.fullMarks.reset();
              this.getAllSubjects();
            }
          },
          (error) => {
            this.toastr.error('Something Went Wrong!', '', {
              positionClass: 'toast-custom1',
            });
          }
        );
      }
    });
  }

  public getAllSubjects() {
    this.adminService.getSubjects().subscribe(
      (data) => {
        this.subjects = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public getClassesForSubject() {
    this.adminService.getClassesForSubject(this.selSub.id).subscribe(
      (data: any) => {
        this.selectedClasses = data;
        this.classIndexes = this.selectedClasses.map((clOb) => clOb.id);
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public getAllClasses() {
    this.adminService.getClasses().subscribe(
      (data) => {
        this.classes = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public onSelectSubject(event: any, sub: any) {
    if (event.target.checked) {
      this.getClassesForSubject();
    }
  }
  public onSelectClass(event: any, cl: any) {
    if (event.target.checked) {
      this.selectedClasses.push(cl);
      this.classIndexes.push(cl.id);
    } else {
      this.selectedClasses = this.selectedClasses.filter(
        (clas: any) => clas.id != cl.id
      );
      this.classIndexes = this.classIndexes.filter((c: any) => c != cl.id);
    }
  }

  public assignSubToClass() {
    this.subClassWrapper.classes = this.selectedClasses;
    if (this.selSub.id > 0) {
      this.subClassWrapper.subjects[0] = this.selSub;
      this.adminService.assignSubToClass(this.subClassWrapper).subscribe(
        (data) => {
          if (data == 1) {
            this.toastr.success('subject mapped to selected classes', '', {
              positionClass: 'toast-custom1',
            });
          } else {
            this.toastr.success('No class is mapped with subject!', '', {
              positionClass: 'toast-custom1',
            });
          }
        },
        (error) => {
          this.toastr.error('Something Went Wrong!', '', {
            positionClass: 'toast-custom1',
          });
        }
      );
    } else {
      this.toastr.warning('subject is not selected', '', {
        positionClass: 'toast-custom1',
      });
    }
  }
}
