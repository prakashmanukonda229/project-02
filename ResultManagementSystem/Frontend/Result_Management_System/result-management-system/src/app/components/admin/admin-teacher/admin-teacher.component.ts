import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-teacher',
  templateUrl: './admin-teacher.component.html',
  styleUrls: ['./admin-teacher.component.css'],
})
export class AdminTeacherComponent implements OnInit {
  selActive: boolean = true;
  selInactive: boolean = false;

  subjects: any[] = [];
  activeTeachers: any[] = [];
  inactiveTeachers: any[] = [];

  constructor(
    private adminService: AdminService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isAdminLoggedIn')) {
      this.adminService.processLogout();
    } else {
      var val = localStorage.getItem('isInactive');
      if (val == 'True') {
        this.selActive = false;
        this.selInactive = true;
      } else {
        this.selActive = true;
        this.selInactive = false;
      }
      this.getAllActiveTeachers();
      this.getAllInactiveTeachers();
    }
  }
///////////////////////////////////////////////////////////////////
  onClickActive() {
    localStorage.removeItem('isInactive');
    this.selActive = true;
    this.selInactive = false;
    this.getAllActiveTeachers();
  }

  onClickInactive() {
    localStorage.setItem('isInactive', 'True');
    this.selActive = false;
    this.selInactive = true;
    this.getAllInactiveTeachers();
  }

  getAllActiveTeachers() {
    this.adminService.getAllActiveTeachers().subscribe(
      (data: any) => {
        this.activeTeachers = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getAllInactiveTeachers() {
    this.adminService.getAllInactiveTeachers().subscribe(
      (data: any) => {
        this.inactiveTeachers = data;
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
