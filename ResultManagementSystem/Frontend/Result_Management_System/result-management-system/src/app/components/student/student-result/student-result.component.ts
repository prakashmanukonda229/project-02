import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/Models/teacher';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { Subject } from 'src/app/Models/subject';

@Component({
  selector: 'app-student-result',
  templateUrl: './student-result.component.html',
  styleUrls: ['./student-result.component.css'],
})
export class StudentResultComponent implements OnInit {
  student: Student = new Student();
  markList: number[] = [];
  subjectList: Subject[] = [];
  resList: String[] = [];
  sumActual: number = 0;
  sumTotal: number = 0;
  percent!: String;
  cnt:number=0;

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private roter: Router
  ) {}

  ngOnInit(): void {

    const js = sessionStorage.getItem('studentSession');
    this.student = js ? JSON.parse(js) : null;
    this.getResultForStudent();

    if (!localStorage.getItem('foo')) { 
      localStorage.setItem('foo', 'no reload') 
      location.reload() 
    } else {
      localStorage.removeItem('foo') 
    }

  }

  getResultForStudent() {
    this.adminService.getResultForStudent(this.student.id).subscribe(
      (value: any) => {
        this.markList = value.markList;
        this.subjectList = value.subjects;
        this.resList = value.subjectNames;
        this.sumActual = this.markList[this.subjectList.length];
        this.sumTotal = this.markList[this.subjectList.length + 1];
        this.percent = ((this.sumActual * 100) / this.sumTotal).toFixed(2);
      },
      (error) => {
        this.toastr.error('Server Down!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  backToLogin(){
    sessionStorage.removeItem('studentSession')
    this.roter.navigate(['/student'])
  }
}
