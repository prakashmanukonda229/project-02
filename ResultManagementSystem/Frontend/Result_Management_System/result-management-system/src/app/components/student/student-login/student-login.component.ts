import { StudentResultComponent } from './../student-result/student-result.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { timeout } from 'rxjs';
import { Class1, Student } from 'src/app/Models/teacher';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css'],
})
export class StudentLoginComponent implements OnInit {
  classList: Class1[] = [];
  tempClassList: Class1[] = [];
  student: Student = new Student();
  studentLoc: Student = new Student();

  roll!: number;
  motherName: String = '';
  clsOne: any;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    this.getAllClasses();
  }

  onSubmit() {
    this.student.motherName = this.motherName;
    this.student.rollNo = this.roll;
    this.student.class1 = this.tempClassList[0];

    this.adminService.doStudentLogin(this.student).subscribe(
      (value: any) => {
        if (value == 2) {
          this.toastr.info(
            'Result is not yet published for class ' +
              this.tempClassList[0].name,
            '',
            { positionClass: 'toast-custom1' }
          );
        } else if (value == 1) {
          this.toastr.warning('Please enter Correct Details!', '', {
            positionClass: 'toast-custom1',
          });
        } else if (value == 0) {
          this.loadStudent(this.student);
        }
      },
      (error) => {
        this.toastr.error('Server Down!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }



  loadStudent(stu: Student) {
    this.adminService
      .loadStudent(stu)
      .subscribe(
        (arg:any) =>{
  sessionStorage.setItem('studentSession', JSON.stringify(arg)),
   this.router.navigate(['/student/result']);
        }
      );
  }

  public getAllClasses() {
    this.adminService.getClasses().subscribe(
      (value: any) => {
        this.classList = value;
      },
      (error) => {
        this.toastr.error('Error in loading classes', '', {
          positionClass: 'toast-custom1',
        });
      }
    );

    this.clsOne = '--Select--';
  }

  onSelectClass(event: any) {
    this.tempClassList = this.classList.filter(
      (cl) => cl.name == event.target.value
    );
  }
}
