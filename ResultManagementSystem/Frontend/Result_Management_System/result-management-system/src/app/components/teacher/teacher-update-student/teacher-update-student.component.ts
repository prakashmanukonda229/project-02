import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Student } from 'src/app/Models/teacher';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-update-student',
  templateUrl: './teacher-update-student.component.html',
  styleUrls: ['./teacher-update-student.component.css'],
})
export class TeacherUpdateStudentComponent implements OnInit {
  student: Student = new Student();
  studentId!: number;

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      this.studentId = this.route.snapshot.params['stuId'];
      this.getStudentDetails();
    }
  }

  updateStudent() {
    this.teacherService.updateStudent(this.student).subscribe(
      (value: any) => {
        if (value == 0) {
          this.toastr.success('Student details updated!', '', {
            positionClass: 'toast-custom1',
          });
          this.getStudentDetails();
        } else {
          this.toastr.error('Problem occured while updating Student', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Problem occured while adding Student to class', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getStudentDetails() {
    this.teacherService.getStudentDetails(this.studentId).subscribe(
      (value: any) => {
        this.student = value;
      },
      (error) => {
        this.toastr.error('Problem occured while adding Student to class', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  back() {
    this.router.navigate([
      "'/teacher/dashboard/view-students/'+${this.student.class1.id}",
    ]);
  }
}
