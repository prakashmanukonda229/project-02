import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TeacherClass } from 'src/app/Models/teacher';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.css'],
})
export class MobileComponent implements OnInit {
  constructor(
    private teacherService: TeacherServiceService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  teacher: TeacherClass = new TeacherClass();

  ngOnInit(): void {
    
  }

  public checkMobile() {
    this.teacherService.checkMobile(this.teacher).subscribe(
      (data) => {
        if (data.username != null) {
          if (data.secQue == 'Name of country I would like to visit') {
            sessionStorage.setItem(
              'tempQue',
              'Which country you would like to visit?'
            );
          } else if (data.secQue == 'Name of my first pet') {
            sessionStorage.setItem(
              'tempQue',
              "What is/was your first pet's name?"
            );
          } else if (data.secQue == 'Name of my favorite actor') {
            sessionStorage.setItem('tempQue', 'Who is your favorite actor?');
          } else {
            sessionStorage.setItem('tempQue', data.secQue);
          }

          sessionStorage.setItem('tempAns', data.secAns);
          sessionStorage.setItem('tempUsername', data.username);

          this.router.navigate(['teacher/login/mobile/security-answer']);
        } else {
          this.toastr.warning('Mobile number not found in records', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
