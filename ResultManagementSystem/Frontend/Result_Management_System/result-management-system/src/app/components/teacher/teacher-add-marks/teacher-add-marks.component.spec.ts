import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherAddMarksComponent } from './teacher-add-marks.component';

describe('TeacherAddMarksComponent', () => {
  let component: TeacherAddMarksComponent;
  let fixture: ComponentFixture<TeacherAddMarksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeacherAddMarksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeacherAddMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
