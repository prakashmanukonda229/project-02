import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherViewMarksComponent } from './teacher-view-marks.component';

describe('TeacherViewMarksComponent', () => {
  let component: TeacherViewMarksComponent;
  let fixture: ComponentFixture<TeacherViewMarksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeacherViewMarksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeacherViewMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
