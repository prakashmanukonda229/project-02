import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherUpdateStudentComponent } from './teacher-update-student.component';

describe('TeacherUpdateStudentComponent', () => {
  let component: TeacherUpdateStudentComponent;
  let fixture: ComponentFixture<TeacherUpdateStudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeacherUpdateStudentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeacherUpdateStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
