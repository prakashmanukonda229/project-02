import { SubClassWrapper } from './../../../Models/teacher';
import { Subject } from './../../../Models/subject';
import { Class1, Result, Student } from 'src/app/Models/teacher';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-add-marks',
  templateUrl: './teacher-add-marks.component.html',
  styleUrls: ['./teacher-add-marks.component.css'],
})
export class TeacherAddMarksComponent implements OnInit {
  classId!: number;
  subjectId!: number;
  class1!: Class1;
  subject: Subject = new Subject();
  resultList: Result[] = [];
  wrapper: SubClassWrapper = new SubClassWrapper();
  valid: boolean = true;
  resultListLocal: Result[] = [];
  confirmed: boolean = false;

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      this.classId = this.route.snapshot.params['cId'];
      this.getClassDetails();
      this.subjectId = this.route.snapshot.params['sId'];
      this.valid = true;
    }
  }

  processSaving() {
    this.resultListLocal = this.resultList.filter(
      (res) => (res.marks > res.subject.maxMarks) || (res.marks < 0)
    );
    if (this.resultListLocal.length > 0) {
      this.valid = false;
    } else {
      this.valid = true;
    }
    if (this.valid) {
      this.saveResult();
    } else {
      this.toastr.error(
        'Marks cannot be greater than maximum subject marks or less than 0!',
        '',
        { positionClass: 'toast-custom1' }
      );
    }
  }

  saveResult() {
    this.wrapper.results = this.resultList;
    this.wrapper.confirmed = this.confirmed;
    this.wrapper.classes[0] = this.class1;
    this.wrapper.subjects[0] = this.subject;
    this.teacherService.saveResult(this.wrapper).subscribe(
      (value: any) => {
        if (value == 0) {
          this.toastr.success('Marks are added', '', {
            positionClass: 'toast-custom1',
          });
          this.getResultList();
        }
      },
      (error) => {
        this.toastr.error('problem occured while saving Marks', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getClassDetails() {
    this.adminService.getClassDetails(this.classId).subscribe(
      (value: any) => {
        this.class1 = value;
        this.getResultList();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getSubjectDetails() {
    this.teacherService.getSubjectDetails(this.subjectId).subscribe(
      (value: any) => {
        this.subject = value;
        this.getConfirmed();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  onSelectCheck(event: any) {
    if (event.target.checked) {
      this.confirmed = true;
    } else {
      this.confirmed = false;
    }
  }

  getConfirmed() {
    this.teacherService.getConfirmed(this.classId, this.subjectId).subscribe(
      (value: any) => {
        this.confirmed = value.confirmed;
      },
      (error) => {
        this.toastr.error('problem occured while checking confirmation!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getResultList() {
    this.teacherService.getResultList(this.classId, this.subjectId).subscribe(
      (value: any) => {
        this.resultList = value
        this.getSubjectDetails();
      },
      (error) => {
        this.toastr.error('problem occured while loading student Results', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  back() {
    this.router.navigate(['/teacher/dashboard/add-marks']);
  }
}
