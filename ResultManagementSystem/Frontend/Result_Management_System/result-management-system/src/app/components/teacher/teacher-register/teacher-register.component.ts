import { TeacherServiceService } from './../../../services/teacher-service.service';
import { TeacherClass } from './../../../Models/teacher';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-teacher-register',
  templateUrl: './teacher-register.component.html',
  styleUrls: ['./teacher-register.component.css'],
})
export class TeacherRegisterComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();
  questions = [
    'Name of my favorite actor',
    'Name of my first pet',
    'Name of country I would like to visit',
  ];
  constructor(
    private teacherService: TeacherServiceService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  public onRegister() {
    this.teacherService.registerTeacher(this.teacher).subscribe(
      (data) => {
        if (data) {
          this.toastr.success('Registration Successful', '', {
            positionClass: 'toast-custom1',
          });
          this.router.navigate(['/teacher/login']);
        } else {
          this.toastr.warning('Username or Mobile Number already in Use', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
