import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TeacherClass } from 'src/app/Models/teacher';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();

  constructor(
    private teacherService: TeacherServiceService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
      this.teacher.username = sessionStorage.getItem('tempUsername') || '';
  }

  public resetPassword() {
    this.teacherService.resetPassword(this.teacher).subscribe(
      (data) => {
        if (data) {
          this.toastr.success('Password Reset Successfully', '', {
            positionClass: 'toast-custom1',
          });
          sessionStorage.clear();
          this.router.navigate(['teacher/login']);
        } else {
          this.toastr.error('Unexpected error!', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
