import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Class1, TeacherClass } from 'src/app/Models/teacher';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-add-students-class',
  templateUrl: './add-students-class.component.html',
  styleUrls: ['./add-students-class.component.css'],
})
export class AddStudentsClassComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();
  classes: Class1[] = [];
  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      const js = sessionStorage.getItem('teacherSession');
      this.teacher = js ? JSON.parse(js) : null;
      if (this.route.snapshot.url.toString() == 'add-student') {
        this.getClassesToAddStudent();
      } else if (this.route.snapshot.url.toString() == 'add-marks') {
        this.getClassesToAddMarks();
      } else {
        this.getAllClasses();
      }
    }
  }

  public getClassesToAddStudent() {
    this.adminService.getClassesForClassTeacher(this.teacher).subscribe(
      (value: any) => {
        this.classes = value;
      },
      (error) => {
        this.toastr.error(
          'Error in loading classes for Teacher to add Student',
          '',
          { positionClass: 'toast-custom1' }
        );
      }
    );
  }

  public getClassesToAddMarks() {
    this.teacherService.getClassesToAddMarks(this.teacher).subscribe(
      (value: any) => {
        this.classes = value;
      },
      (error) => {
        this.toastr.error('Error in loading classes for add marks', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public getAllClasses() {
    this.adminService.getClasses().subscribe(
      (value: any) => {
        this.classes = value;
      },
      (error) => {
        this.toastr.error('Error in loading all classes', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
