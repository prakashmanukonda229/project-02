import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-teacher-navigation',
  templateUrl: './teacher-navigation.component.html',
  styleUrls: ['./teacher-navigation.component.css'],
})
export class TeacherNavigationComponent implements OnInit {
  constructor(private router: Router, private toastr: ToastrService) {}

  ngOnInit(): void {}

  public processLogout() {
    sessionStorage.removeItem('isTeacherLoggedIn');
    this.toastr.success('You are logged out!!', '', {
      positionClass: 'toast-custom1',
    });
    this.router.navigate(['']);
  }
}
