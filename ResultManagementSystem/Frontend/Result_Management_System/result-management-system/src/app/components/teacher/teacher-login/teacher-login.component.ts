import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TeacherClass } from 'src/app/Models/teacher';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-login',
  templateUrl: './teacher-login.component.html',
  styleUrls: ['./teacher-login.component.css'],
})
export class TeacherLoginComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();

  constructor(
    private teacherService: TeacherServiceService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  public onLogin() {
    this.teacherService.loginTeacher(this.teacher).subscribe(
      (data) => {
        if (data == 1) {
          this.toastr.success('Login Successful!', '', {
            positionClass: 'toast-custom1',
          });
          this.teacherService
            .getTeacher(this.teacher)
            .subscribe((arg) =>
              sessionStorage.setItem('teacherSession', JSON.stringify(arg))
            );

          sessionStorage.setItem('isTeacherLoggedIn', 'true');
          this.router.navigate(['/teacher/dashboard']);
        } else if (data == 0) {
          this.toastr.info('Your account is not yet Activated by Admin!', '', {
            positionClass: 'toast-custom1',
          });
        } else {
          this.toastr.warning(
            'Please enter correct Username and Password',
            '',
            { positionClass: 'toast-custom1' }
          );
        }
      },
      (error) => {
        this.toastr.error('Something Went Wrong!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
