import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStudentsClassComponent } from './add-students-class.component';

describe('AddStudentsClassComponent', () => {
  let component: AddStudentsClassComponent;
  let fixture: ComponentFixture<AddStudentsClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddStudentsClassComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddStudentsClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
