import { Subject } from './../../../Models/subject';
import { Class1, Student } from 'src/app/Models/teacher';
import { AdminService } from './../../../services/admin.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-view-marks',
  templateUrl: './teacher-view-marks.component.html',
  styleUrls: ['./teacher-view-marks.component.css'],
})
export class TeacherViewMarksComponent implements OnInit {
  classId!: number;
  class1!: Class1;
  finResForSemList: number[][] = [[]];
  studenList: Student[] = [];
  subjectList: Subject[] = [];

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      this.classId = this.route.snapshot.params['cId'];
      this.getClassDetails();
    }
  }

  getClassDetails() {
    this.adminService.getClassDetails(this.classId).subscribe(
      (value: any) => {
        this.class1 = value;
        this.getResultForClassAndSem();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getResultForClassAndSem() {
    this.teacherService.getResultForClass(this.classId).subscribe(
      (value: any) => {
        this.finResForSemList = value.resultOfClassForSem;
        this.studenList = value.studentList;
        this.subjectList = value.subjects;
      },
      (error) => {
        this.toastr.error('problem occured while loading class Result', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
