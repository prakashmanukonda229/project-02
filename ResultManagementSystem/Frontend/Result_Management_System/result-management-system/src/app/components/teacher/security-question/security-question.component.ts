import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TeacherClass } from 'src/app/Models/teacher';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-security-question',
  templateUrl: './security-question.component.html',
  styleUrls: ['./security-question.component.css'],
})
export class SecurityQuestionComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();

  constructor(
    private teacherService: TeacherServiceService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
      this.teacher.secQue = sessionStorage.getItem('tempQue') || '';
  }

  public checkAnswer() {
    if (this.teacher.secAns == sessionStorage.getItem('tempAns') || '') {
      this.router.navigate([
        'teacher/login/mobile/security-answer/reset-password',
      ]);
    } else {
      this.toastr.warning(
        'Your answer does not match with records!',
        '',
        { positionClass: 'toast-custom1' }
      );
    }
  }
}
