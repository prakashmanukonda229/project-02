import { Class1, Student, TeacherClass } from 'src/app/Models/teacher';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-view-students',
  templateUrl: './teacher-view-students.component.html',
  styleUrls: ['./teacher-view-students.component.css']
})
export class TeacherViewStudentsComponent implements OnInit {
  classId!:any
  class1!:Class1
  studentList:Student[] = []
  permittedclasses:Class1[] = []
  teacher:TeacherClass = new TeacherClass()
  hasPermission:Boolean = false
  tempclasses:Class1[] = []

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService
  ) { }

  ngOnInit(): void {
    if(!sessionStorage.getItem("isTeacherLoggedIn")){
      this.teacherService.processTLogout()
      }else{
    this.classId = this.route.snapshot.params['cId'];
    this.getClassDetails();
    const js = sessionStorage.getItem('teacherSession');
    this.teacher = js ? JSON.parse(js) : null;
    this.getClassesToAddStudent()
      }
    
  }

  public getClassesToAddStudent() {
    this.adminService.getClassesForClassTeacher(this.teacher).subscribe(
      (value: any) => {
        this.permittedclasses = value;
        this.checkIfHasPermission();
      },
      (error) => {
        this.toastr.error('Error in loading classes for Teacher to add Student','',{ positionClass: 'toast-custom1'});
      }
    );
  }

  checkIfHasPermission(){
    this.tempclasses =  this.permittedclasses.filter(perm=> perm.id == this.classId)
    if(this.tempclasses.length > 0){
      this.hasPermission = true
    }else{
      this.hasPermission = false
    }
  }

  getClassDetails(){
    this.adminService.getClassDetails(this.classId).subscribe(
      (value:any) => {
        this.class1 = value;
        this.getStudentList();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!','',{ positionClass: 'toast-custom1'});
      }
    );
  }


  deleteStudent(sId:number){
    this.teacherService.deleteStudent(sId).subscribe(
      (value:any) => {
        this.getStudentList();
        this.toastr.info('Student deleted!','',{ positionClass: 'toast-custom1'});
      },
      (error) => {
        this.toastr.warning('Student not found','',{ positionClass: 'toast-custom1'});
      }
    );
  }

  getStudentList(){
    this.teacherService.studentsOfClass(this.class1.id).subscribe(
      (value:any) => {
        this.studentList = value;
      },
      (error) => {
        this.toastr.error('problem occured while loading Students','',{ positionClass: 'toast-custom1'});
      }
    );
  }
}
