import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/Models/formmodel';
import { Class1, TeacherClass } from 'src/app/Models/teacher';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacher-profile.component.html',
  styleUrls: ['./teacher-profile.component.css'],
})
export class TeacherProfileComponent implements OnInit {
  teacher: TeacherClass = new TeacherClass();
  permissions: Permission[] = [];
  classes: Class1[] = [];
  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private teacherService: TeacherServiceService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      const js = sessionStorage.getItem('teacherSession');
      this.teacher = js ? JSON.parse(js) : null;
      this.getPermissionsForTeacher();
      this.getClassesForClassTeacher();
    }
  }

  public getPermissionsForTeacher() {
    this.adminService.getPermissionsForTeacher(this.teacher).subscribe(
      (value: any) => {
        this.permissions = value;
      },
      (error) => {
        this.toastr.error('Ploblem in loading permissions', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  public getClassesForClassTeacher() {
    this.adminService.getClassesForClassTeacher(this.teacher).subscribe(
      (value: any) => {
        this.classes = value;
      },
      (error) => {
        this.toastr.error('Problem occured while loading classes!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
