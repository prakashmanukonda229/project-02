import { Subject } from './../../../Models/subject';
import { AdminService } from 'src/app/services/admin.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';
import { Component, OnInit } from '@angular/core';
import { Class1, TeacherClass } from 'src/app/Models/teacher';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css'],
})
export class SubjectComponent implements OnInit {
  classId!: any;
  class1!: Class1;
  subjectList: Subject[] = [];
  teacher: TeacherClass = new TeacherClass();

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      const js = sessionStorage.getItem('teacherSession');
      this.teacher = js ? JSON.parse(js) : null;

      this.classId = this.route.snapshot.params['cId'];
      this.getClassDetails();
    }
  }

  getClassDetails() {
    this.adminService.getClassDetails(this.classId).subscribe(
      (value: any) => {
        this.class1 = value;
        this.getSubjectList();
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  getSubjectList() {
    this.teacherService
      .getSubjectList(this.class1.id, this.teacher.id)
      .subscribe(
        (value: any) => {
          this.subjectList = value;
        },
        (error) => {
          this.toastr.error('problem occured while loading Subjects', '', {
            positionClass: 'toast-custom1',
          });
        }
      );
  }
}
