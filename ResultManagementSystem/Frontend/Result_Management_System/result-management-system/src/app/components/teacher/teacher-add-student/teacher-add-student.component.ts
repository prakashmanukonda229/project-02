import { Class1, Student } from './../../../Models/teacher';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { TeacherServiceService } from 'src/app/services/teacher-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-teacher-add-student',
  templateUrl: './teacher-add-student.component.html',
  styleUrls: ['./teacher-add-student.component.css'],
})
export class TeacherAddStudentComponent implements OnInit {
  classId!: number;
  class1!: Class1;
  student: Student = new Student();
  @ViewChild('motherName') motherName: any;
  @ViewChild('rollNo') rollNo: any;
  @ViewChild('fullName') fullName: any;

  constructor(
    private teacherService: TeacherServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    if (!sessionStorage.getItem('isTeacherLoggedIn')) {
      this.teacherService.processTLogout();
    } else {
      this.classId = this.route.snapshot.params['cId'];
      this.getClassDetails();
    }
  }

  getClassDetails() {
    this.adminService.getClassDetails(this.classId).subscribe(
      (value: any) => {
        this.class1 = value;
        this.student.class1 = this.class1;
      },
      (error) => {
        this.toastr.error('problem occured while loading Class details!', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  processAddStudent() {
    this.teacherService.checkStudentPresence(this.student).subscribe(
      (value: any) => {
        if (value == 0) {
          this.addStudent();
        } else {
          Swal.fire({
            // title:'',
            text: 'Student with the same roll number already present for this class. Do you want to update details ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel',
          }).then((result) => {
            if (result.value) {
              this.updateStudent();
            }
          });
        }
      },
      (error) => {
        this.toastr.error(
          "Problem occured while checking Student's presence",
          '',
          { positionClass: 'toast-custom1' }
        );
      }
    );
  }

  addStudent() {
    this.teacherService.addStudent(this.student).subscribe(
      (value: any) => {
        if (value == 0) {
          this.toastr.success('Student added to class', '', {
            positionClass: 'toast-custom1',
          });
          this.fullName.reset();
          this.rollNo.reset();
          this.motherName.reset();
        } else {
          this.toastr.warning(
            'Student already added!',
            '',
            { positionClass: 'toast-custom1' }
          );
        }
      },
      (error) => {
        this.toastr.error('Problem occured while adding Student to class', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }

  updateStudent() {
    this.teacherService.updateStudent(this.student).subscribe(
      (value: any) => {
        if (value == 0) {
          this.toastr.success('Student details updated!', '', {
            positionClass: 'toast-custom1',
          });
          this.fullName.reset();
          this.rollNo.reset();
          this.motherName.reset();
        } else {
          this.toastr.error('Problem occured while updating Student', '', {
            positionClass: 'toast-custom1',
          });
        }
      },
      (error) => {
        this.toastr.error('Problem occured while adding Student to class', '', {
          positionClass: 'toast-custom1',
        });
      }
    );
  }
}
