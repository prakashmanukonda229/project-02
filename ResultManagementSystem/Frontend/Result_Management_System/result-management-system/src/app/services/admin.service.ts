import { TeacherClass, Student } from 'src/app/Models/teacher';
import { Class1, SubClassWrapper } from './../Models/teacher';
import { Subject } from './../Models/subject';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import baseUrl from '../helper/baseurl';
import { Admin } from '../Models/Admin';
import { Permission } from '../Models/formmodel';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {}

  // Admin Login
  public adminLoginCall(admin: Admin): Observable<any> {
    return this.httpClient.post(`${baseUrl}/admin/login`, admin);
  }

  public addSubject(subject: Subject): Observable<any> {
    return this.httpClient.post(`${baseUrl}/subject/add`, subject);
  }

  public updateSubject(subject: Subject): Observable<any> {
    return this.httpClient.put(`${baseUrl}/subject/update`, subject);
  }

  public deleteSubject(sId: any): Observable<any> {
    return this.httpClient.delete(`${baseUrl}/subject/delete/${sId}`);
  }

  public getSubjects(): Observable<any> {
    return this.httpClient.get(`${baseUrl}/subject/getall`);
  }

  public addClass(class1: Class1): Observable<any> {
    return this.httpClient.post(`${baseUrl}/class/add`, class1);
  }

  public updateClass(class1: Class1): Observable<any> {
    return this.httpClient.put(`${baseUrl}/class/update`, class1);
  }

  public deleteClass(cId: any): Observable<any> {
    return this.httpClient.delete(`${baseUrl}/class/delete/${cId}`);
  }

  public getClasses(): Observable<any> {
    return this.httpClient.get(`${baseUrl}/class/getall`);
  }

  public getClassesForTeacher(teacher: TeacherClass): Observable<any> {
    return this.httpClient.get(`${baseUrl}/class/getForTeacher/${teacher.id}`);
  }

  loadStudent(student: Student) {
    return this.httpClient.post(`${baseUrl}/student/load`, student);
  }

  public assignSubToClass(subClassWrapper: any): Observable<any> {
    return this.httpClient.post(
      `${baseUrl}/permission/assSubClass`,
      subClassWrapper
    );
  }

  public getAllActiveTeachers() {
    return this.httpClient.get(`${baseUrl}/teacher/getAllActive`);
  }

  public getAllInactiveTeachers() {
    return this.httpClient.get(`${baseUrl}/teacher/getAllInactive`);
  }

  public deactivateTeacher(teacher: TeacherClass) {
    return this.httpClient.put(`${baseUrl}/teacher/deactivate`, teacher);
  }

  public activateTeacher(teacher: TeacherClass) {
    return this.httpClient.put(`${baseUrl}/teacher/activate`, teacher);
  }

  public updateClassTeacher(subClassWrapper: SubClassWrapper) {
    return this.httpClient.put(
      `${baseUrl}/class/classTeacher`,
      subClassWrapper
    );
  }

  public getClassesForClassTeacher(teacher: TeacherClass) {
    return this.httpClient.get(`${baseUrl}/class/forTeacher/${teacher.id}`);
  }

  public addPermission(permission: Permission) {
    return this.httpClient.post(`${baseUrl}/permission/add`, permission, {
      responseType: 'text',
    });
  }

  public getSubjectForClasses(clas: Class1) {
    return this.httpClient.get(`${baseUrl}/permission/subForClass/${clas.id}`);
  }

  public getPermissionsForTeacher(teacher: TeacherClass) {
    return this.httpClient.get(
      `${baseUrl}/permission/getPerForTeacher/${teacher.id}`
    );
  }

  public deletePermission(perm: Permission) {
    return this.httpClient.delete(`${baseUrl}/permission/delete/${perm.id}`);
  }

  public deleteTeacher(teacher: TeacherClass) {
    return this.httpClient.delete(`${baseUrl}/teacher/delete/${teacher.id}`);
  }

  public getClassDetails(id: any) {
    return this.httpClient.get(`${baseUrl}/class/get/${id}`);
  }

  public getTeachersOfClass(id: any) {
    return this.httpClient.get(`${baseUrl}/permission/getTSMap/${id}`);
  }

  public getClassesForSubject(cId: any) {
    return this.httpClient.get(`${baseUrl}/permission/getBySubId/${cId}`);
  }

  public getPermissions(cId: any) {
    return this.httpClient.get(`${baseUrl}/permission/getByClassId/${cId}`);
  }

  public publish(class1: any) {
    return this.httpClient.put(`${baseUrl}/class/publish`, class1);
  }

  public revert(class1: any) {
    return this.httpClient.put(`${baseUrl}/class/revert`, class1);
  }

  public getData() {
    return this.httpClient.get(`${baseUrl}/admin/analytics`);
  }

  public doStudentLogin(student: Student) {
    return this.httpClient.post(`${baseUrl}/student/login`, student);
  }

  public getResultForStudent(stId: number) {
    return this.httpClient.get(`${baseUrl}/result/getForStudent/${stId}`);
  }

  public addSubToClass(wrapper: SubClassWrapper) {
    return this.httpClient.post(`${baseUrl}/permission/addSubToClass`, wrapper);
  }

  public processLogout() {
    this.router.navigate(['/admin']);
    this.toastr.warning('Session expired!! Please login again!', '', {
      positionClass: 'toast-custom1',
    });
  }
}
