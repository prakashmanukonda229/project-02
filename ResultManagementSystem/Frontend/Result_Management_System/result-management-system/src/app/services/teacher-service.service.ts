import { Class1 } from 'src/app/Models/teacher';
import { Student, TeacherClass, SubClassWrapper } from './../Models/teacher';
import { Injectable } from '@angular/core';
import baseUrl from '../helper/baseurl';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class TeacherServiceService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {}

  // Register Teacher
  public registerTeacher(teacher: TeacherClass): Observable<any> {
    return this.httpClient.post(`${baseUrl}/teacher/registration`, teacher);
  }
  // Login Teacher
  public loginTeacher(teacher: TeacherClass): Observable<any> {
    return this.httpClient.post(`${baseUrl}/teacher/login`, teacher);
  }

  // Get Teacher
  public getTeacher(teacher: TeacherClass): Observable<any> {
    return this.httpClient.post(`${baseUrl}/teacher/getTeacher`, teacher);
  }

  public getTeacherById(teacher: TeacherClass): Observable<any> {
    return this.httpClient.get(`${baseUrl}/teacher/getTeacherById/${teacher.id}`);
  }

  public checkMobile(teacher: TeacherClass): Observable<any> {
    return this.httpClient.post(`${baseUrl}/teacher/checkMobile`, teacher);
  }

  public resetPassword(teacher: TeacherClass): Observable<any> {
    return this.httpClient.put(`${baseUrl}/teacher/resetPassword`, teacher);
  }

  public getAllUsers() {
    return this.httpClient.get(`${baseUrl}/member/getAllUsers`);
  }

  public getClassesToAddMarks(teacher: TeacherClass) {
    return this.httpClient.get(
      `${baseUrl}/permission/getClassesToAddMarks/${teacher.id}`
    );
  }

  public addStudent(student: Student) {
    return this.httpClient.post(`${baseUrl}/student/add`, student);
  }

  public checkStudentPresence(student: Student) {
    return this.httpClient.post(`${baseUrl}/student/check`, student);
  }

  public updateStudent(student: Student) {
    return this.httpClient.put(`${baseUrl}/student/update`, student);
  }

  public studentsOfClass(classId: number) {
    return this.httpClient.get(`${baseUrl}/student/getAllByClass/${classId}`);
  }

  public getSubjectList(class1Id: number, teacherId: number) {
    return this.httpClient.get(
      `${baseUrl}/permission/getSubjectsToAddMarks/${class1Id}/${teacherId}`
    );
  }

  public getSubjectDetails(sid: any) {
    return this.httpClient.get(`${baseUrl}/subject/get/${sid}`);
  }

  public getResultList(clId: number, subId: number) {
    return this.httpClient.get(
      `${baseUrl}/result/getForClassSubject/${clId}/${subId}`
    );
  }

  public saveResult(wrapper: SubClassWrapper) {
    return this.httpClient.post(`${baseUrl}/result/saveResult`, wrapper);
  }

  public getResultForClass(classId: number) {
    return this.httpClient.get(
      `${baseUrl}/result/getResultForClass/${classId}`
    );
  }

  public getStudentDetails(studentId: number) {
    return this.httpClient.get(`${baseUrl}/student/get/${studentId}`);
  }

  public deleteStudent(sId: number) {
    return this.httpClient.delete(`${baseUrl}/student/delete/${sId}`);
  }

  getConfirmed(classId: number, subjectId: number) {
    return this.httpClient.get(
      `${baseUrl}/permission/getConfirmed/${classId}/${subjectId}`
    );
  }

  public processTLogout() {
    this.router.navigate(['/teacher/login']);
    this.toastr.warning('Session expired!! Please login again!', '', {
      positionClass: 'toast-custom1',
    });
  }
}
